<!DOCTYPE HTML>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<script async src="//46494.tctm.co/t.js"></script>
		<title><?php wp_title( '|' ); ?></title>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="google-site-verification" content="DecqxZOBTboO2z7FwIlDoXAiRS2OXTkL68v_zzKbqGA" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon.ico"/>
		<?php wp_head(); ?>
		<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
		<!--[if lt IE 9]>
            <script src="<?php bloginfo('template_url'); ?>/js/vendor/respond.min.js"></script> <script src="<?php bloginfo('template_url'); ?>/js/vendor/selectivizr-min.js"></script>
        <![endif]-->
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-1.6.4.min.js"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.rss.js"></script>
    <script>
    jQuery(function($) {
  $("#rss-feeds").rss("http://feeds.feedburner.com/SigmaThermalBlog", {
    limit: 2,
    entryTemplate: '<li class="rss-bucket"><a class="rss-link" href="{url}">{title}</a><p class="rss-content">{shortBodyPlain}...</p><a class="rss-morelink" href="{url}">Read More</a></li>' 
  })
})
    </script>
<script> jQuery(document).ready(function() {twitterFetcher.fetch( '674263438659481600', 'twitter-feeds', 2, true);});</script>

<!-- Bing  --> 
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5104803"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=5104803&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>
	</head>
	
	<body <?php body_class(); ?>>
		<!-- Google Tag Manager --> <noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-PSMHKQ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-PSMHKQ');</script> <!-- End Google Tag Manager --> 