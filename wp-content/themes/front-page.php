<?php

	/*
		Template Name: Front Page
	*/
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>


<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


 
        <section class="site-intro">
            <div class="inner-wrap">
                <h1 class="site-intro-h1">
                    <span>Industry Leader in</span>
                    Industrial Process Heating Systems, Automation, Services and Parts
                </h1>
            </div>
                
            <div class="flexslider product-carousel">
                <ul class="slides">
                    <li>
                        <a class="product-item" href="<?php bloginfo('url'); ?>/fuel-gas-conditioning/">
                            
                            <div class="product-inner-wrap">
                                <h3 class="product-header">
                                   Fuel Gas Conditioning
                                </h3> 
                                <p class="product-description">
                                    Fuel gas conditioning is a critical optimization process for the removal of damaging impurities from gas streams.
                                </p>
                            </div>
                            <figure class="product-figure" style="background-image:url(<?php bloginfo('template_url'); ?>/img/homethumb-tank-suction-heating.jpg);">
                            </figure>
                            <!--<img src="http://rpm.thomaswebs.net/sigma-thermal/wp-content/themes/sigma-default/img/homethumb-tank-suction-heating.jpg" alt="Fuel Gas Conditioning" class="product-img">-->
                        </a>
                        <a class="product-item" href="<?php bloginfo('url'); ?>/process-air-heating/">
                            
                            <div class="product-inner-wrap">
                                <h3 class="product-header">
                                   Air Heating
                                </h3> 
                                <p class="product-description">v
                                   In combustion turbine systems, air inlet heating is a key component for optimizing combustion efficiency and maintaining equipment longevity.
                                </p>
                            </div>
                            <figure class="product-figure" style="background-image:url(<?php bloginfo('template_url'); ?>/img/thumb-air-heating.jpg);">
                            </figure>
                        </a>
                        <a class="product-item" href="<?php bloginfo('url'); ?>/regeneration-gas-heating/">
                            
                            <div class="product-inner-wrap">
                                <h3 class="product-header">
                                   Regeneration Gas Heating
                                </h3> 
                                <p class="product-description">
                                   Sigma Thermal manufactures high performance regeneration gas heating systems using a variety of technologies, fuel sources, and burner options.
                                </p>
                            </div>
                            <figure class="product-figure" style="background-image:url(<?php bloginfo('template_url'); ?>/img/thumb-regeneration-gas.jpg);">
                            </figure>
                        </a>
                        <a class="product-item" href="<?php bloginfo('url'); ?>/tank-suction-heating/">
                            
                            <div class="product-inner-wrap">
                                <h3 class="product-header">
                                   Tank & Suction Heating
                                </h3> 
                                <p class="product-description">
                                   Tank heating systems are key components for flow control, viscosity consistency, and protecting against frozen and broken pipelines.
                                </p>
                            </div>
                            <figure class="product-figure" style="background-image:url(<?php bloginfo('template_url'); ?>/img/thumb-jacketed-reactor.jpg);">
                            </figure>
                        </a>
                        
                    </li>
                    <li>
                        <a class="product-item" href="<?php bloginfo('url'); ?>/ovens-and-fryers-process-heating">
                            
                            <div class="product-inner-wrap">
                                <h3 class="product-header">
                                  Ovens & Fryers 
                                </h3> 
                                <p class="product-description">
                                    Heating systems used in industrial food processing, such as ovens and fryers, must provide consistent, uniform temperatures to maximize quality and yields.
                                </p>
                            </div>
                            <figure class="product-figure" style="background-image:url(<?php bloginfo('template_url'); ?>/img/homethumb-regeneration-gas-heating.jpg);">
                            </figure>
                        </a>
                        <a class="product-item" href="<?php bloginfo('url'); ?>/molding-extrusion-process-heating">
                            
                            <div class="product-inner-wrap">
                                <h3 class="product-header">
                                  Molding & Extrusion 
                                </h3> 
                                <p class="product-description">
                                    Precise temperature control is critical to the final quality of molded or extruded components. 
                                </p>
                            </div>
                            <figure class="product-figure" style="background-image:url(<?php bloginfo('template_url'); ?>/img/thumb-molding-extrusion.jpg);">
                            </figure>
                        </a>
                        <a class="product-item" href="<?php bloginfo('url'); ?>/viscosity-reduction-heating">
                            
                            <div class="product-inner-wrap">
                                <h3 class="product-header">
                                   Viscosity Reduction 
                                </h3> 
                                <p class="product-description">
                                    Pumping crude oil requires significantly more power when the oil is cold. Heating the oil to reduce the viscosity increases the mobility. 
                                </p>
                            </div>
                            <figure class="product-figure" style="background-image:url(<?php bloginfo('template_url'); ?>/img/thumb-viscosity-reduction.jpg);">
                            </figure>
                        </a>
                        <a class="product-item" href="<?php bloginfo('url'); ?>/freeze-protection-systems">
                            
                            <div class="product-inner-wrap">
                                <h3 class="product-header">
                                   Freeze Protection 
                                </h3> 
                                <p class="product-description">
                                    Freeze protection is critical to a variety of industrial processes; from protecting water storage tanks to keeping heavy equipment operating during cold weather.
                                </p>
                            </div>
                            <figure class="product-figure" style="background-image:url(<?php bloginfo('template_url'); ?>/img/thumb-freeze-protection.jpg);">
                            </figure>
                        </a>
                        
                    </li>
                   <li>
                        <a class="product-item" href="<?php bloginfo('url'); ?>/ammonia-heating/">
                            
                            <div class="product-inner-wrap">
                                <h3 class="product-header">
                                  Ammonia Heating 
                                </h3> 
                                <p class="product-description">
                                    Sigma Thermal manufactures all types of heaters to manage NH3 in its liquid and vaporized during production and/or distribution.                                        </p>
                            </div>
                            <figure class="product-figure" style="background-image:url(<?php bloginfo('template_url'); ?>/img/thumb-ammonia-heating.jpg);">
                            </figure>
                        </a>
                        <a class="product-item" href="<?php bloginfo('url'); ?>/steam-superheating-systems">
                            
                            <div class="product-inner-wrap">
                                <h3 class="product-header">
                                  Steam Superheating 
                                </h3> 
                                <p class="product-description">
                                    Superheating ensures that a steam flow through a turbine or engine always remains a compressible gas that will not condense. 
                                </p>
                            </div>
                            <figure class="product-figure" style="background-image:url(<?php bloginfo('template_url'); ?>/img/thumb-steam-superheating.jpg);">
                            </figure>
                        </a>
                        <a class="product-item" href="<?php bloginfo('url'); ?>/amine-glycol-reboilers/">
                            
                            <div class="product-inner-wrap">
                                <h3 class="product-header">
                                  Amine & Glycol Reboilers 
                                </h3> 
                                <p class="product-description">
                                    In the amine gas treating process, a reboiler is used to remove the acid gas and recycle the rich amine mixture into a lean stream that is again ready for processing.
                                </p>
                            </div>
                            <figure class="product-figure" style="background-image:url(<?php bloginfo('template_url'); ?>/img/thumb-amine-glycol.jpg);">
                            </figure>
                        </a>
                        <a class="product-item" href="<?php bloginfo('url'); ?>/jacketed-reactor-heating-coding/">
                            
                            <div class="product-inner-wrap">
                                <h3 class="product-header">
                                   Jacketed Reactor
                                </h3> 
                                <p class="product-description">
                                    Thermal fluid systems can provide constant processing temperatures up to 800°F and operate at lower pressures than steam jackets.
                                </p>
                            </div>
                            <figure class="product-figure" style="background-image:url(<?php bloginfo('template_url'); ?>/img/thumb-jacketed-reactor.jpg);">
                            </figure>
                        </a>
                    </li>
                </ul>
            </div>

<p id="last"></p>
<div id="slidebox"><a class="close">close</a>

<!--HubSpot Call-to-Action Code -->
<span class="hs-cta-wrapper" id="hs-cta-wrapper-f75288d3-f691-4c33-9d14-e47833e5c69d">
    <span class="hs-cta-node hs-cta-f75288d3-f691-4c33-9d14-e47833e5c69d" id="hs-cta-f75288d3-f691-4c33-9d14-e47833e5c69d">
        <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->
        <a href="http://cta-redirect.hubspot.com/cta/redirect/379517/f75288d3-f691-4c33-9d14-e47833e5c69d" ><img class="hs-cta-img" id="hs-cta-img-f75288d3-f691-4c33-9d14-e47833e5c69d" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/379517/f75288d3-f691-4c33-9d14-e47833e5c69d.png"  alt="New Call-to-action"/></a>
    </span>
    <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
    <script type="text/javascript">
        hbspt.cta.load(379517, 'f75288d3-f691-4c33-9d14-e47833e5c69d');
    </script>
</span>
<!-- end HubSpot Call-to-Action Code -->

</div>

        </section>
        <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/cta-module' ) ); ?>
         <!--
         <section>
            <div class="inner-wrap">
                <div class="cta-banner-dark">
                    <span class="col-9">
                    <h2 class="cta-banner-header">Selecting a Thermal Fluid Heater</h2>
                    <p class="cta-banner-body">With do many heater designs, and many different heater manufacturers, it can be overwhelming to try to select a thermal fluid heater. How does you determining the right solution? Download our FREE eBook now to find out.</p>
                    
                </span>
                <a href="http://industrial.sigmathermal.com/selecting-the-right-thermal-fluid-heater-download" target="_blank" class="col-3 col-last btn-orange-l cta-banner-btn">Download eBook</a>
                </div>
            </div>
        </section>
        -->

<div id="rss-feeds"></div>


<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>