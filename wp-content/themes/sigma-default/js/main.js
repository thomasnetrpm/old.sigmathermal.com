//PNG Fallback

if(!Modernizr.svg) {
  var images = $('img[data-png-fallback]');
  images.each(function(i) {
    $(this).attr('src', $(this).data('png-fallback'));
  });
}

//Toggle Boxes
$(document).ready(function() {
  $('body').addClass('js');
  var $activatelink = $('.activate-link');
  
$activatelink.click(function() {
	var $this = $(this);
	$this.toggleClass('active').next('div').toggleClass('active');
	return false;
});
   
}); 

//Responsive Navigation
$(document).ready(function() {
	  $('body').addClass('js');
	  var $menu = $('#menu'),
	  	  $menulink = $('.menu-link'),
	  	  $menuTrigger = $('.has-subnav > a');
	
	$menulink.click(function(e) {
		e.preventDefault();
		$menulink.toggleClass('active');
		$menu.toggleClass('active');
	});
	
	$menuTrigger.click(function(e) {
		e.preventDefault();
		var $this = $(this);
		$this.toggleClass('active').next('ul').toggleClass('active');
	});
	
});


//Lightbox
$(document).ready(function() {
  $('.lightbox').magnificPopup({type:'image'});
});
$(document).ready(function() {
	$('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,

		fixedContentPos: false
	});
});

//Show More
$(document).ready(function() {
	$( ".showmore" ).after( "<p><a href='#' class='show-more-link'>More</a></p>");
	var $showmorelink = $('.showmore-link');  
	$showmorelink.click(function() {
		var $this = $(this);
		var $showmorecontent = $('.showmore');
		$this.toggleClass('active');
		$showmorecontent.toggleClass('active');
		return false;
	});
});

//Show More
$(document).ready(function() {
	var $expandlink = $('.headexpand');  
	$expandlink.click(function() {
		var $this = $(this);
		var $showmorecontent = $('.showmore');

		$this.toggleClass('active').next().toggleClass('active');
		$showmorecontent.toggleClass('active');
		return false;
	});
});

 //Flexslider    
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: false,
    controlNav: false
  });
});

$(window).load(function() {
  $('.dest-carousel').flexslider({
    animation: "slide",
    animationLoop: false,
    controlNav: false,
    itemWidth: 200,
    itemMargin:10,  
    minItems:1,
    maxItems:4,
  });
});

//Slide in CTA
$(function() {
  var slidebox = $('#slidebox');
  if (slidebox) {
    $(window).scroll(function(){
        var distanceTop = $('#last').offset().top - $(window).height();
        if  ($(window).scrollTop() > distanceTop)
            slidebox.animate({'right':'0px'},300);
        else
            slidebox.stop(true).animate({'right':'-430px'},100);
    });
    $('#slidebox .close').on('click',function(){
        $(this).parent().remove();
    });
  }
});


