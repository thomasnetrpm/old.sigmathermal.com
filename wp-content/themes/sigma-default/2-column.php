<?php

    /*
        Template Name: 2-column
    */
?>


<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<!--Site Content-->
<section class="site-content" role="main">
<div class="inner-wrap">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


            <h1><?php the_title(); ?></h1>
<article class="site-content-primary col-10">            
            
<?php the_content(); ?>
                
                  
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>                 

           
            




<?php endwhile; ?>
</article>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/sidebar' ) ); ?>
</div><!--inner-wrap END-->

<?php if(get_field('slide_cta') ): ?>
     <p id="last"></p>
           <div id="slidebox"><a class="close">close</a>
          <?php the_field('slide_cta'); ?>
<!-- end HubSpot Call-to-Action Code -->
</div>

<?php endif; ?>


</section><!--site-content END-->   

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>