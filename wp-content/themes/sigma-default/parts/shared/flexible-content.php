
<?php
 
// check if the flexible content field has rows of data
if( have_rows('flexible_content') ):
	echo '<section class="additional-content">';
     // loop through the rows of data
    while ( have_rows('flexible_content') ) : the_row(); ?>


	<?php
	if( get_row_layout() == 'spec_table' ): ?>
	
<?php if( get_sub_field('picture_repeat')): ?>	
	
	
	 	<article class="clearboth clearfix spec-table-wrap">
	 	<?php if( get_sub_field('section_header')): ?>
					<h2><?php the_sub_field('section_header'); ?></h2>
				<?php endif; ?>
				<?php if( get_sub_field('section_subtext')): ?>
					<p><?php the_sub_field('section_subtext'); ?></p>
				<?php endif; ?>
	 		<div class="spec-table col-6">
				
				
				<section class="table colored-rows">
				<?php if( get_sub_field('table_header')): ?>
					<h3 class="spec-table-header"><?php the_sub_field('table_header'); ?></h3>
				<?php endif; ?>
				
					<?php if( have_rows('spec_table_row') ): 
		
					while ( have_rows('spec_table_row') ) : the_row(); ?>
					<div class="table-row">
					<div class="cell spec-col-1">
						<p><strong>
						<?php the_sub_field('spec_header'); ?>
						</strong></p>
					</div>
					<div class="cell spec-col-2"><p>
						<?php the_sub_field('spec_body'); ?>
					</p></div>
					</div>
					<?php endwhile; ?>
					<?php endif; ?>
				
				
				</section>
			</div>
			<figure class="spec-table-img col-6 col-last">	
			
	<?php if( have_rows('picture_repeat') ):		
	while ( have_rows('picture_repeat') ) : the_row(); ?>
				
		<?php 	
			$image = wp_get_attachment_image_src(get_sub_field('picture'), 'medium');
			$imagelarge = wp_get_attachment_image_src(get_sub_field('picture'), 'large');
		 ?>
	 
	
		<a href="<?php echo $imagelarge[0]; ?>" class="lightbox">
				 <img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_field('image_test')) ?>" />
			<?php if( get_sub_field('sub_title')): ?>
				<figcaption><?php the_sub_field('sub_title'); ?></figcaption>
			<?php endif; ?>
		</a>
		
	
	
		
	<?php endwhile; ?>
	<?php endif; ?>
	<small class="clearboth">(click to enlarge)</small>
	</figure>
		</article>
<?php else : ?>
	
				<?php if( get_sub_field('section_header')): ?>
					<h2><?php the_sub_field('section_header'); ?></h2>
				<?php endif; ?>
				<?php if( get_sub_field('section_subtext')): ?>
					<p><?php the_sub_field('section_subtext'); ?></p>
				<?php endif; ?>
				
				<section class="table colored-rows">
				<?php if( get_sub_field('table_header')): ?>
					<h3 class="spec-table-header"><?php the_sub_field('table_header'); ?></h3>
				<?php endif; ?>
				
					<?php if( have_rows('spec_table_row') ): 
		
					while ( have_rows('spec_table_row') ) : the_row(); ?>
					<div class="table-row">
					<div class="cell spec-col-1">
						<p><strong>
						<?php the_sub_field('spec_header'); ?>
						</strong></p>
					</div>
					<div class="cell spec-col-2"><p>
						<?php the_sub_field('spec_body'); ?>
					</p></div>
					</div>
					<?php endwhile; ?>
					<?php endif; ?>
				
				
				</section>

<?php endif; ?>	
					
				
				
			

	
	<?php
	elseif( get_row_layout() == 'full_width_cta' ): ?>


				<div class="cta-banner-light bottom-baseline">
                    <span class="col-9">
                    <h2 class="cta-banner-header"><?php the_sub_field('section_header'); ?></h2>
                    <p class="cta-banner-body"><?php the_sub_field('section_body'); ?></p>
                    
                </span>
                <a href="<?php the_sub_field('url'); ?>" class="col-3 col-last btn-orange-l cta-banner-btn"><?php the_sub_field('cta_button'); ?></a>
                </div>


 			



<?php
	elseif( get_row_layout() == 'product_introduction' ): ?>

<section class="product-intro row">
    <?php if ( has_post_thumbnail()) : ?>
        <figure class="product-intro-img  col-4">
            <?php if ( has_post_thumbnail()) {
   $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');
   echo '<a href="' . $large_image_url[0] . '"  class="lightbox" >';
   echo get_the_post_thumbnail($post->ID, 'medium'); 
   echo '</a>';
}
?>


<?php if( have_rows('additional_product_img') ):        
                while ( have_rows('additional_product_img') ) : the_row(); ?>
<?php   
    $image = wp_get_attachment_image_src(get_sub_field('product_img'), 'thumbnail');
    $imagelarge = wp_get_attachment_image_src(get_sub_field('product_img'), 'large');
 ?>
 <a href="<?php echo $imagelarge[0]; ?>" class="lightbox product-intro-img-additional">
 <img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_field('image_test')) ?>" />
 </a>

<?php endwhile; ?>
<?php endif; ?>
 <small class="clearboth">(click to enlarge)</small>
            
           
            
        </figure>
    <?php endif; ?>
    
    <div class="product-intro-text <?php if(get_sub_field('additional_info')) {echo 'col-6';} else {echo 'col-8 last';} ?>">
        <?php the_sub_field('product_info'); ?>                          
    </div>

   <?php if( get_sub_field('additional_info')): ?>
<section class="graybox bottom-baseline product-intro-additional col-2 last">
     
            <?php the_sub_field('additional_info'); ?>
        
<?php if( get_sub_field('cta_url')): ?>
    <a href="<?php the_sub_field('cta_url'); ?>" class="btn-orange-l product-intro-additional-cta"><?php the_sub_field('cta_text'); ?></a>

<?php endif; ?>

    </section>
<?php endif; ?> 

</section>
 



<?php elseif( get_row_layout() == 'content_columns' ): ?>
 	<?php if( get_sub_field('section_header')): ?>
			<h2><?php the_sub_field('section_header'); ?></h2>
		<?php endif; ?>
		<?php if( get_sub_field('section_subheader')): ?>
			<p><?php the_sub_field('section_subheader'); ?></p>
		<?php endif; ?>
			<section class="<?php if (get_sub_field('number_columns') == '2') {
					echo 'rows-of-2';
				} else if (get_sub_field('number_columns') == '3') {
				        echo 'rows-of-3';
				} else if (get_sub_field('number_columns') == '4') {
				        echo 'rows-of-4';
				}
				?>">

         	<?php if( have_rows('content') ): while ( have_rows('content') ) : the_row(); ?>
				<div><?php the_sub_field('column'); ?></div>
			<?php endwhile; ?>
			<?php endif; ?>
			</section>
			
			
			
<?php
elseif( get_row_layout() == 'picture_grid' ): ?>

		<?php if( get_sub_field('section_header')): ?>
			<h2><?php the_sub_field('section_header'); ?></h2>
		<?php endif; ?>
		<?php if( get_sub_field('section_subtext')): ?>
			<p><?php the_sub_field('section_subtext'); ?></p>
		<?php endif; ?>
<section class="mobile-rows-of-2 <?php if (get_sub_field('number_columns') == '2') {
					echo 'rows-of-2';
				} else if (get_sub_field('number_columns') == '3') {
				        echo 'rows-of-3';
				} else if (get_sub_field('number_columns') == '4') {
				        echo 'rows-of-4';
				}
				?>">
<?php if( get_sub_field('lightbox')): ?>	
	<?php if( have_rows('picture_repeat') ):		
	while ( have_rows('picture_repeat') ) : the_row(); ?>
				
		<?php 	
			$image = wp_get_attachment_image_src(get_sub_field('picture'), 'thumbnail');
			$imagelarge = wp_get_attachment_image_src(get_sub_field('picture'), 'large');
		 ?>
	 
	
		<a href="<?php echo $imagelarge[0]; ?>" class="lightbox">
				 <img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_field('image_test')) ?>" />
			<?php if( get_sub_field('sub_title')): ?>
				<figcaption><?php the_sub_field('sub_title'); ?></figcaption>
			<?php endif; ?>
		</a>
		
	
	
		
	<?php endwhile; ?>
	<?php endif; ?>
	
<?php else : ?>
	<?php if( have_rows('picture_repeat') ):		
	while ( have_rows('picture_repeat') ) : the_row(); ?>
				
		<?php 	
			$image = wp_get_attachment_image_src(get_sub_field('picture'), 'thumbnail');
			$imagelarge = wp_get_attachment_image_src(get_sub_field('picture'), 'full');
		 ?>
	 
	
		<figure>
			 <img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_field('image_test')) ?>" />
		<?php if( get_sub_field('sub_title')): ?>
			<figcaption><?php the_sub_field('sub_title'); ?></figcaption>
		<?php endif; ?>
	</figure>
		
	
	
		
	<?php endwhile; ?>
	<?php endif; ?>
	
<?php endif; ?>
</section>


<?php
elseif( get_row_layout() == 'product_grid' ): ?>

		<?php if( get_sub_field('section_header')): ?>
			<h2><?php the_sub_field('section_header'); ?></h2>
		<?php endif; ?>
		<?php if( get_sub_field('section_subtext')): ?>
			<p><?php the_sub_field('section_subtext'); ?></p>
		<?php endif; ?>
	
<?php if( get_sub_field('carousel')): ?>	
<div class="dest-carousel">
        <ul class="slides">
	<?php if( have_rows('product_repeat') ):		
	while ( have_rows('product_repeat') ) : the_row(); ?>
				
		<?php 	
			$image = wp_get_attachment_image_src(get_sub_field('picture'), 'thumbnail');
			$imagelarge = wp_get_attachment_image_src(get_sub_field('picture'), 'full');
		 ?>
	 
	
		<li>
		<a class="product-item lightbox" href="<?php echo $imagelarge[0]; ?>"> 
			<span class="dest-product-img">
			 <img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_field('image_test')) ?>" />
			</span>
			
			<?php if( get_sub_field('product_header')): ?>
				<h3 class="product-header-wrap">
					<span class="product-header"><?php the_sub_field('product_header'); ?></span>
				</h3>
			<?php endif; ?>
			<?php if( get_sub_field('product_subheader')): ?>
				<span class="product-subheader">
					<?php the_sub_field('product_subheader'); ?>
				</span>
			<?php endif; ?>
		
	</a>
		</li>
		
	
	
		
	<?php endwhile; ?>
	<?php endif; ?>
        </ul>
</div>

<?php else : ?>
<section class="grid-4 row">
	<?php if( have_rows('product_repeat') ):		
	while ( have_rows('product_repeat') ) : the_row(); ?>
				
		<?php 	
			$image = wp_get_attachment_image_src(get_sub_field('picture'), 'thumbnail');
			$imagelarge = wp_get_attachment_image_src(get_sub_field('picture'), 'full');
		 ?>
	 
	
		<a class="product-item" href="<?php the_sub_field('product_url'); ?>"> 
			<span class="product-img">
			 <img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_field('image_test')) ?>" />
			</span>
			
			<h3 class="product-header-wrap">
				<span class="product-header"><?php the_sub_field('product_header'); ?></span>
				<?php if( get_sub_field('product_subheader')): ?>
					<span class="product-subheader">
						<?php the_sub_field('product_subheader'); ?>
					</span>
				<?php endif; ?>
			</h3>
		
	</a>
		
		
	<?php endwhile; ?>
	<?php endif; ?>
</section>
	<?php endif; ?>








 
 			
<?php
	elseif( get_row_layout() == 'text_media' ): ?>
			
			<?php if( get_sub_field('section_subtext')): ?>
				<p><?php the_sub_field('section_subtext'); ?></p>
			<?php endif; ?>
			
         	<article class="clearfix">
        		
        		<div class="col-3of9">
        			<?php the_sub_field('media'); ?>
        		</div>
        		<div class="col-6of9 col-last">
        		<?php if( get_sub_field('section_header')): ?>
				<h2><?php the_sub_field('section_header'); ?></h2>
			<?php endif; ?>
        			<?php the_sub_field('text'); ?>
        		</div>
        		
 			</article>
 			<?php if( get_sub_field('divider')): ?>
 				<hr>
			<?php endif; ?>







	<?php elseif( have_rows('picture_repeat') ):		
	while ( have_rows('picture_repeat') ) : the_row(); ?>
				
		<?php 	
			$image = wp_get_attachment_image_src(get_sub_field('picture'), 'thumbnail');
			$imagelarge = wp_get_attachment_image_src(get_sub_field('picture'), 'full');
		 ?>
	 
	
		<div class="media-object">
			 <figure class="media-object-img">
			 	<img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_field('image_test')) ?>" />
			 </figure>
		
			<div class="media-object-body">
				<?php if( get_sub_field('media_title')): ?>
					<h3 class="media-object-header"><?php the_sub_field('media_title'); ?></h3>
				<?php endif; ?>
				<?php if( get_sub_field('media_body')): ?>
					<p><?php the_sub_field('media_body'); ?></p>
				<?php endif; ?>
				
				
			</div>
		</div>
	
		
	
	
		
	<?php endwhile; ?>
	<?php endif; ?>
	







<?php endwhile; ?>
<?php endif; ?>



