<section class="black-module">
            <div class="inner-wrap">
                <a href="http://industrial.sigmathermal.com/brochures-form" class="grayboxlink bottom-baseline-mobile bottom-baseline m-col-6 l-col-3 ico-resource" target="_blank">
                    <h3 class="grayboxlink-head">Helpful Resources</h3>
                    <p class="grayboxlink-bd">Download a complete set of print-quality PDF brochures.</p>
                </a>
                <a href="http://industrial.sigmathermal.com/videos-form" target="_blank" class="grayboxlink bottom-baseline-mobile bottom-baseline m-col-6 m-col-last l-col-3 ico-video">
                    <h3 class="grayboxlink-head">Watch Videos</h3>
                    <p class="grayboxlink-bd">Register for access to our online video demonstrations.</p>
                </a>
                <a href="http://industrial.sigmathermal.com/request-a-quote-no-product-specified" class="grayboxlink bottom-baseline-mobile bottom-baseline m-col-6 l-col-3 ico-rfq" target="_blank">
                    <h3 class="grayboxlink-head">Submit RFQ</h3>
                    <p class="grayboxlink-bd">Fill out a quick online form and we'll send you a quote.</p>
                </a>
                <a href="<?php bloginfo('url'); ?>/parts" class="bottom-baseline-mobile bottom-baseline grayboxlink m-col-6 l-col-3 col-last ico-cog">
                    <h3 class="grayboxlink-head">Find Parts</h3>
                    <p class="grayboxlink-bd">Our engineers will help you find the parts and services you need.</p>
                </a>
                
               <!--  <a href="http://industrial.sigmathermal.com/selecting-the-right-thermal-fluid-heater-download" class="grayboxlink bottom-baseline-mobile bottom-baseline-medium m-col-6 l-col-3 ico-download" target="_blank">
                    <h3 class="grayboxlink-head">How Do You Select the Right Thermal Fluid Heater?</h3>
                    <p class="grayboxlink-bd">FREE eBook download.</p>
                </a>
                <a href="http://industrial.sigmathermal.com/product-application-quick-reference-guide" class="grayboxlink bottom-baseline-mobile bottom-baseline-medium m-col-6 m-col-last l-col-3 ico-download" target="_blank">
                    <h3 class="grayboxlink-head">Find Which Product Line Suits Your Application Best</h3>
                    <p class="grayboxlink-bd">FREE eBook download.</p>
                </a>
                <a href="http://industrial.sigmathermal.com/considerations-of-electric-thermal-fluid-heating-systems-ebook-download" class="grayboxlink bottom-baseline-mobile bottom-baseline-medium m-col-6 l-col-3 ico-download" target="_blank">
                    <h3 class="grayboxlink-head">10 Essential Tips for Buying Electric Thermal Fluid Heating Systems</h3>
                    <p class="grayboxlink-bd">FREE eBook download.</p>
                </a>
                <a href="http://industrial.sigmathermal.com/mact-emissions-compliance-requirements-download" class="bottom-baseline-mobile bottom-baseline grayboxlink m-col-6 l-col-3 col-last ico-download">
                    <h3 class="grayboxlink-head">Process Heaters Emissions Requirements Guide</h3>
                    <p class="grayboxlink-bd">FREE eBook download.</p>
                </a> -->
            </div>
        </section>