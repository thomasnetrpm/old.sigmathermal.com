<!--Site Header-->
<header class="site-header" role="banner">
    <div class="inner-wrap">
        <div class="site-header-top">
	        <a href="<?php bloginfo('url'); ?>" class="site-logo"><img src="<?php bloginfo('template_url'); ?>/img/sigma-thermal-logo.png" alt="Site Logo"></a>
	        <div class="utility-nav">
	            <a href="http://industrial.sigmathermal.com/iso-quality-certification" class="ico-iso" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-iso.png" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-iso.png" alt="ISO Certified"></a>
	            <div class="social-wrap">
	                <a href="https://www.facebook.com/SigmaThermal" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-facebook.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-facebook.png" alt="Facebook"></a>
	                <a href="https://twitter.com/SigmaThermal" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-twitter.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-twitter.png" alt="Twitter"></a>
	                <a href="https://plus.google.com/104393776006751491260/posts" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-googleplus.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-googleplus.png" alt="Google Plus"></a>
	                <a href="https://www.linkedin.com/company/sigma-thermal-inc." target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-linkedin.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-linkedin.png" alt="LinkedIn"></a>
	                <a href="http://blog.sigmathermal.com/blog" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-blog.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-blog.png" alt="Blog"></a>
	            </div>
	           
	            <nav class="secondary-nav">
	            	<?php wp_nav_menu(array('menu' => 'Utility Nav','container' => '','items_wrap' => '<ul>%3$s</ul>',)); ?> 
	              <!--  <a href="#">About</a>
	                <a href="">Sigma Reps</a>
	                <a href="">Contact</a>
	                <a href="" class="white-link">English</a>
	                <a href="" class="white-link">Spanish</a>-->
	              <span class="sun-ph">P (770) 427-5770</span>
	            </nav>
	           
	            <a href="#menu" class="mobile-nav mobile menu-link active">
	                <span>Menu</span>
	            </a>
	        </div>  
        
        </div>   
        <div class="site-header-bottom">
        <nav id="menu" class="menu clearfix" role="navigation"> 
            <?php wp_nav_menu(array('menu' => 'Primary Nav','container' => '','items_wrap' => '<ul class="level-1">%3$s</ul>',)); ?> 
        </nav>
        </div>
    </div>
</header>


    