
	


<!--Site Footer-->
<section class="grain-module bordertop-gradient">
    <div class="inner-wrap footerlinks">
                <div class="footerlinks-col-1">
                    <h5 class="footerlinks-head">
                        <a href="<?php bloginfo('url'); ?>/thermal-fluid-systems" title="Thermal Fluid Systems">Thermal Fluid Systems</a>
                    </h5>
                    <ul>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/thermal-fluid-systems#thermal-oil-heating-systems" title="Thermal Oil Heating Systems">Thermal Oil Heating Systems</a></li>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/thermal-fluid-systems#water-glycol-heating-systems" title="Glycol Reboiler Heating Systems">Water-Glycol Heating Systems</a></li>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/thermal-fluid-systems#hc-2-thermal-fluid-heater" title="HC-2 Thermal Fluid Heaters">HC-2 Thermal Fluid Heater</a></li>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/thermal-fluid-systems#hc-2-thermal-fluid-heater" title="HC-1 Thermal Fluid Systems">HC-1 Thermal Fluid Heater</a></li>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/thermal-fluid-systems#complete-thermal-fluid-systems" title="Complete Thermal Fluid Heaters &amp; Systems">Complete Thermal Fluid Systems</a></li>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/thermal-fluid-systems#custom-engineering" title="Custom Engineered Fired Heater Systems">Custom Engineering</a></li>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/thermal-fluid-systems#economizers" title="Thermal Oil Economizers">Economizers</a></li>
                    </ul>
                </div>



                <div class="footerlinks-col-2">
                    <h5 class="footerlinks-head">
                        <a href="<?php bloginfo('url'); ?>/direct-fired-heaters" title="Direct Fired Heaters">Direct Fired Heaters</a>
                    </h5>

                    <ul>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/direct-fired-heaters#convection-heater" title="Convection Heaters">Convection Heater</a></li>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/direct-fired-heaters#radiant-convective-heater" title="Radiant-Convection Heaters">Radiant—Convective Heater</a></li>
                    </ul>

                    <h5 class="footerlinks-head">
                        <a href="<?php bloginfo('url'); ?>/process-bath-heaters" title="Process Bath Heater">Process Bath Heaters</a>
                    </h5>

                    <ul>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/process-bath-heaters#water-bath-heater" title="Water Bath Heater">Water Bath Heater</a></li>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/process-bath-heaters#salt-bath-heater" title="Salt Bath Heaters">Salt Bath Heater</a></li>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/process-bath-heaters#weir-bath-heater" title="Weir Bath Heaters">Weir Bath Heater</a></li>
                    </ul>

                    <h5 class="footerlinks-head">
                        <a href="<?php bloginfo('url'); ?>/electric-process-heaters" title="
                Electric Process Heaters">Electric Process Heaters</a>
                    </h5>

                    <ul>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/electric-process-heaters#immersion-heater" title="Immersion Heater">Immersion Heater</a></li>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/electric-process-heaters#circulation-heater" title="Circulation Heater">Circulation Heater</a></li>
                    </ul>
                </div>


                <div class="footerlinks-col-3">
                    <h5 class="footerlinks-head">
                        <a href="<?php bloginfo('url'); ?>/biomass-energy-systems" title="Biomass-Fired Energy Systems">Biomass Energy Systems</a>
                    </h5>

                    <ul>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/biomass-energy-systems#biomass-fired-combustion-systems" title="Biomass Fired Combustion System">Biomass Fired Combustion Systems</a></li>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/biomass-energy-systems#biomass-fired-hot-gas-generators" title="Biomass Fired Combustion Systems">Biomass Fired Hot Gas Generators</a></li>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/biomass-energy-systems#dust-collection-and-ash-handling" title="Dust Collection &amp; Ash Handling Options">Dust Collection and Ash Handling</a></li>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/biomass-energy-systems#fuel-handling-systems" title="Fuel Handling System">Fuel Handling Systems</a></li>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/biomass-energy-systems#biomass-equipment" title="Biomass Equipment">Biomass Equipment</a></li>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/biomass-energy-systems#biomass-fired-thermal-fluid-heaters" title="Biomass Fired Thermal Fluid Heaters">Biomass Fired Thermal Fluid Heaters</a></li>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/biomass-energy-systems#biomass-fired-steam-boilers" title="Biomass Fixed Steam Boilers">Biomass Fired Steam Boilers</a></li>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/biomass-energy-systems#biomass-control-systems" title="Biomass Control Systems">Biomass Control Systems</a></li>
                    </ul>
                </div>


                <div class="footerlinks-col-4 col-last">
                    <h5 class="footerlinks-head">
                        <a href="<?php bloginfo('url'); ?>/waste-heat-recovery" title="Waste Heat Recovery">Waste Heat Recovery</a>
                    </h5>

                    <ul>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/waste-heat-recovery#heat-energy-recovery-audits" title="Heat Energy Recovery Audio">Heat Energy Recovery Audits</a></li>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/waste-heat-recovery#complete-closed-loop-systems" title="Complete Closed Loop  Systems">Complete Closed Loop Systems</a></li>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/waste-heat-recovery#combustion-air-pre-heat-system-caph" title="Combustion Air Pre-Heat System">Combustion Air Pre-Heat System (CAPH)</a></li>
                        <li>
                            <a href="<?php bloginfo('url'); ?>/waste-heat-recovery#high-particulate-systems" title="High Particulate Systems">High Particulate Systems</a></li>
                    </ul>

                    <h5 class="footerlinks-head">
                        <a href="<?php bloginfo('url'); ?>/automation" title="Automated burner management systems &amp; combustion controls">Automation</a></h5>

                    <h5 class="footerlinks-head">
                        <a href="<?php bloginfo('url'); ?>/services" title="Fired Heater Services">Services</a></h5>

                    <h5 class="footerlinks-head">
                        <a href="<?php bloginfo('url'); ?>/parts" title="Heating System Components &amp; Parts">Parts</a></h5>
                </div>
                <div class="footerlinks-col-5">
                    <div class="social-wrap">
                        <a href="https://www.facebook.com/SigmaThermal" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-facebook.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-facebook.png" alt="Facebook"></a>
                        <a href="https://twitter.com/SigmaThermal" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-twitter.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-twitter.png" alt="Twitter"></a>
                        <a href="https://plus.google.com/104393776006751491260/posts" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-googleplus.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-googleplus.png" alt="Google Plus"></a>
                        <a href="https://www.linkedin.com/company/sigma-thermal-inc-" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-linkedin.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-linkedin.png" alt="LinkedIn"></a>
                        <a href="http://blog.sigmathermal.com/blog" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-blog.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-blog.png" alt="Blog"></a>
                    </div>
                    <a class="orange-link" href="http://industrial.sigmathermal.com/iso-quality-certification">ISO 9001:2008 Certified</a>
                </div>
            </div>
        </section>


        <!--Site Footer-->
        <footer class="site-footer" role="contentinfo">
            <div class="inner-wrap">
                <nav class="footer-nav">
                    <?php wp_nav_menu(array('menu' => 'Footer Nav','container' => '','items_wrap' => '<ul>%3$s</ul>',)); ?>
                </nav>
                <p class="footer-address">
                    Sigma Thermal · 4875 Deen Road · Marietta, GA 30066 · P (770) 427-5770  F (678) 254-1762
                </p>
                <p class="footer-copyright">
                    © <?php echo date("Y"); ?> Sigma Thermal - All rights reserved. &emsp;<a href="http://business.thomasnet.com/rpm">Credits</a>
                </p>
            </div>
        </footer>
