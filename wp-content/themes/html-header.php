<!DOCTYPE HTML>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<script async src="//46494.tctm.co/t.js"></script>
		<title><?php wp_title( '|' ); ?></title>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="google-site-verification" content="DecqxZOBTboO2z7FwIlDoXAiRS2OXTkL68v_zzKbqGA" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon.ico"/>
		<?php wp_head(); ?>
		<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
		<!--[if lt IE 9]>
            <script src="<?php bloginfo('template_url'); ?>/js/vendor/respond.min.js"></script> <script src="<?php bloginfo('template_url'); ?>/js/vendor/selectivizr-min.js"></script>
        <![endif]-->
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-1.6.4.min.js"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.rss.js"></script>
    <script>
    jQuery(function($) {
  $("#rss-feeds").rss("http://feeds.feedburner.com/BeltCorporationOfAmericaBlog", {
    limit: 4,
    entryTemplate: '<li class="rss-bucket"><a class="rss-link" href="{url}">{title}</a><p class="rss-content">{shortBodyPlain}...</p></li>' 
  })
})
    </script>
	</head>
	
	<body <?php body_class(); ?>>
