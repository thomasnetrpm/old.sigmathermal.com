<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 4.0
 */
?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<!--Site Content-->
<section class="site-content" role="main">
<div class="inner-wrap">
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


<h1><?php the_title(); ?></h1>
       <article class="site-content-primary col-10"> 
<?php if ('790' == $post->post_parent or is_page('2160') )  : ?>



<?php if ( has_post_thumbnail()) : ?>
<figure class="product-intro-img  col-4of9">
<?php if ( has_post_thumbnail()) {
$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');
echo '<a href="' . $large_image_url[0] . '"  class="lightbox col-6" >';
echo get_the_post_thumbnail($post->ID, 'thumbnail'); 
echo '</a>';
}
?>

<?php if( have_rows('additional_product_img') ):
while ( have_rows('additional_product_img') ) : the_row(); ?>
<?php
$image = wp_get_attachment_image_src(get_sub_field('product_img'), 'thumbnail');
$imagelarge = wp_get_attachment_image_src(get_sub_field('product_img'), 'large');
 ?>
 <a href="<?php echo $imagelarge[0]; ?>" class="lightbox col-6">
 <img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_field('image_test')) ?>" />
 </a>
<?php endwhile; ?>
<?php endif; ?>
<small style="clear:both">(click to enlarge)</small><br>
</figure>
<div class="col-5of9 col-last">
<?php the_content(); ?> 
</div>
<?php else : ?>
<?php the_content(); ?> 
<?php endif; ?>
<?php else : ?>
<?php if ( has_post_thumbnail()) : ?>
<figure class="product-intro-img  col-4of9">
<?php if ( has_post_thumbnail()) {
$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');
echo '<a href="' . $large_image_url[0] . '"  class="lightbox" >';
echo get_the_post_thumbnail($post->ID, 'medium'); 
echo '</a>';
}
?>
<?php if( have_rows('additional_product_img') ):
while ( have_rows('additional_product_img') ) : the_row(); ?>
<?php
$image = wp_get_attachment_image_src(get_sub_field('product_img'), 'thumbnail');
$imagelarge = wp_get_attachment_image_src(get_sub_field('product_img'), 'full');
 ?>
 <a href="<?php echo $imagelarge[0]; ?>" class="lightbox product-intro-img-additional">
 <img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_field('image_test')) ?>" />
 </a>
<?php endwhile; ?>
<?php endif; ?>
<small style="clear:both">(click to enlarge)</small><br>
<!-- <small class="clearboth"><a href="http://www.sigmathermal.com/galleries">View Product Gallery</a></small>
 --></figure>
<div class="col-5of9 col-last">
<?php the_content(); ?> 
</div>
<?php else : ?>
<?php the_content(); ?> 
<?php endif; ?>
<?php endif; ?>
 
 
<?php if (is_page( '9' )) : ?>
<!--Sitemap Page-->
   <ul>
   <?php
   // Add pages you'd like to exclude in the exclude here
   wp_list_pages(
   array(
   'exclude' => '',
   'title_li' => '',
   )
   );
   ?>
   </ul>
<?php endif; ?>  
<div style="clear:both"></div> 
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>                 
       </article>
      
       
       <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/sidebar' ) ); ?>

<?php if (is_page( '98209384' )) : ?>
<!--Products Page-->
<?php global $query_string; $posts = query_posts('&order=ASC&orderby=menu_order&showposts=20&post_parent=16&post_type=page');?>
<div class="product-grid">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<a href="<?php the_permalink(); ?>" class="prd-grd-item">
<figure class="prd-grd-img">
<?php the_post_thumbnail('medium'); ?>
</figure>
<span class="prd-grd-body">
<h3 class="prd-grd-header"><?php the_title(); ?></h3>
</span>
</a>
<?php endwhile; ?>
<?php endif; ?>
</div>
<?php wp_reset_query(); ?>
<?php endif; ?>



<?php endwhile; ?>
</div><!--inner-wrap END-->

<?php if(get_field('slide_cta') ): ?>
<p id="last"></p>
           <div id="slidebox"><a class="close">close</a>
          <?php the_field('slide_cta'); ?>
<!-- end HubSpot Call-to-Action Code -->
</div>

<?php endif; ?>


</section><!--site-content END-->

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>