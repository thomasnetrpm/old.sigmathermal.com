<section class="bloginfo-module">
   <div class="inner-wrap">
          <div class="bi-header">
              <h2 class="bi-header-title">Follow Sigma Thermal</h2><div class="social-wrap">
                    <a href="https://www.facebook.com/SigmaThermal" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-facebook.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-facebook.png" alt="Facebook"></a>
                    <a href="https://twitter.com/SigmaThermal" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-twitter.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-twitter.png" alt="Twitter"></a>
                    <a href="https://plus.google.com/104393776006751491260/posts" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-googleplus.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-googleplus.png" alt="Google Plus"></a>
                    <a href="https://www.linkedin.com/company/sigma-thermal-inc." target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-linkedin.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-linkedin.png" alt="LinkedIn"></a>
                    <a href="http://blog.sigmathermal.com/blog" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-blog.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-blog.png" alt="Blog"></a>
                </div>
          </div> 
          <div class="blog-wrap">
              <div>
                  <h3>Sigma Thermal to Attend the Abu Dhabi International Petroleum Exhibition & Conference (ADIPEC)</h3>
                  <p>Sigma Thermal will be attending the upcoming ADIPEC 2015 event, which takes place from Nov. 9-12. This year the theme is Clean Energy...</p>
                  <a href="#">Read More</a>
              </div>
              <div>
                  <h3>Sigma Thermal to Attend the Abu Dhabi International Petroleum Exhibition & Conference (ADIPEC)</h3>
                  <p>Sigma Thermal will be attending the upcoming ADIPEC 2015 event, which takes place from Nov. 9-12. This year the theme is Clean Energy...</p>
                  <a href="#">Read More</a>
              </div>
              <div>
                  <h3>Sigma Thermal to Attend the Abu Dhabi International Petroleum Exhibition & Conference (ADIPEC)</h3>
                  <p>Sigma Thermal will be attending the upcoming ADIPEC 2015 event, which takes place from Nov. 9-12. This year the theme is Clean Energy...</p>
                  <a href="#">Read More</a>
              </div>
          </div>     
   </div>         
</section>